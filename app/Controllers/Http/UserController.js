'use strict'
const User = use('App/Models/User')
class UserController {

    async login ({ request, response, auth }) {
      const { email, password } = request.all()
      console.log(email,password);
      let result = "";
      try {
        result = await auth.attempt(email, password)
      }catch(e){
          console.log(e);
          response.redirect('/login');
      }
      response.redirect('/');
    }

    show ({ auth, params, response, view }) {
        if (!auth.user) {
            return response.redirect('/login')
        }
        return view.render('home',{user: auth.user});
    }

    async createUser({response, request}){
        const body = request.post();
        const user = new User();
        user.email = body.email;
        user.username = body.email;
        user.name = body.name;
        user.password = body.password;
        await user.save();
        response.redirect(`/dologin?email=${body.email}&password=${body.password}`);
        //response.redirect(`/dologin`,true);
    }

    async logout({auth, response}){
        await auth.logout();
        return response.redirect('/login')
    }
    async signin({view}){
        return view.render('signin')
    }
    async signup({view}){
        return view.render('signup')
    }
  }

module.exports = UserController
