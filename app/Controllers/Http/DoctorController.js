'use strict'
const Database = use('Database');
const Doctor = use('App/Models/Doctor')
class DoctorController {
    async index({request, auth, response, view}){
        if (!auth.user){
            return response.redirect('/login');
        }
        const doctors = await Doctor.all();
        
        return view.render('doctors', { doctors: doctors.toJSON(), user: auth.user });
    }
}

module.exports = DoctorController
