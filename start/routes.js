'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Database = use('Database')
const Route = use('Route')
Route
  .get('users/:id', 'UserController.show')
  .middleware('auth')

Route.get('/','UserController.show');
Route.get('login', 'UserController.signin');
Route.get('signup', 'UserController.signup');
Route.post('logout', 'UserController.logout');
Route.post('dologin', 'UserController.login');
Route.get('dologin', 'UserController.login');
Route.post('docreate', 'UserController.createUser');
Route.resource('doctors','DoctorController');